<?php
/**
 * Outputs the single post content. Displayed by single.php.
 * 
 * @package rosereads
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
    
    <?php 
    // Display featured image only if this is a regular post
    if ('post' === get_post_type()){
        if (has_post_thumbnail()) {
            echo '<div class="single-post-thumbnail clear">';
            echo '<div class="image-shifter">';
            the_post_thumbnail();
            echo '</div>';
            echo '</div>';
        }
    }
    ?>

	<header class="entry-header clear">

		<h1 class="entry-title">
                <?php 
                    the_title(); 
                    if ('review' === get_post_type()){
                        // Echo out the year if this is a review
                        $terms = get_the_terms( $post->ID, 'release_year' );
                        if( $terms && ! is_wp_error( $terms ) ){
                            $term = array_shift($terms);
                            echo ' (' . $term->name . ')';
                        }
                    }
                    ?>
                </h1>

		<div class="entry-meta">
                    
                    <?php 
                    if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) { 
                        echo '<span class="comments-link">';
                        comments_popup_link( __( 'Leave a comment', 'simone' ), __( '1 Comment', 'simone' ), __( '% Comments', 'simone' ) );
                        echo '</span>';
                    }
                    ?>
                    <?php edit_post_link( __( 'Edit', 'simone' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
        
        <div class="entry-content">
                <?php 
                if ('review' === get_post_type()){
                    get_template_part('review', 'info'); 
                }
                ?>
		<?php the_content(); ?>

        <?php rosereads_post_nav(); ?>
        
        </div><!-- .entry-content -->

		</main><!-- #main -->
	</div><!-- #primary -->
    
<?php get_footer(); ?>