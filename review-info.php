<?php
/**
 * Template that displays movie info in a box
 * Kicks in on single comic reviews and index pages
 */
?>

<div class="comic-info clear">
    <?php
    // Do we have a featured image? If so, display it
    if (has_post_thumbnail()){
        echo '<figure class="comic-img">';
        the_post_thumbnail();
        echo '</figure>';
    }
    ?>
  REVIEW INFO
    <div class="comic-data">
        <div class="comic-tax">
            <?php echo get_the_term_list( $post->ID, 'rating', '<h4 class="comic-data-title">Rating</h4>', ', ', '' ); ?> 
        </div>
        <div class="comic-tax">
            <?php echo get_the_term_list( $post->ID, 'series', '<h4 class="comic-data-title">Series</h4>', ', ', '' ); ?> 
        </div>
        <div class="comic-tax">
            <?php echo get_the_term_list( $post->ID, 'creators', '<h4 class="comic-data-title">Creator</h4>', ', ', '' ); ?> 
        </div>
        <div class="comic-tax">
            <?php echo get_the_term_list( $post->ID, 'publishers', '<h4 class="comic-data-title">Publisher</h4>', ', ', '' ); ?> 
        </div>
        <div class="comic-tax">
            <?php echo get_the_term_list( $post->ID, 'formats', '<h4 class="comic-data-title">Format</h4>', ', ', '' ); ?> 
        </div>
        <div class="comic-tax">
            <?php echo get_the_term_list( $post->ID, 'similarto', '<h4 class="comic-data-title">Reminds me of</h4>', ', ', '' ); ?> 
        </div>
    </div>
    <div class="short-review">
        <h4 class="comic-data-title">Quick Review</h4>
        <?php the_excerpt(); ?>
    </div>
</div><!-- .movie-info -->
